diff --git a/src/Test/Inspection/Core.hs b/src/Test/Inspection/Core.hs
index e745975..244362d 100644
--- a/src/Test/Inspection/Core.hs
+++ b/src/Test/Inspection/Core.hs
@@ -44,6 +44,10 @@ import DataCon
 import TyCon (TyCon, isClassTyCon)
 #endif
 
+#if MIN_VERSION_ghc(9,1,0)
+import GHC.Types.Tickish (CoreTickish, GenTickish(..))
+#endif
+
 import qualified Data.Set as S
 import Control.Monad.State.Strict
 import Control.Monad.Trans.Maybe
@@ -83,7 +87,12 @@ slice binds v
     go (Type _)                    = pure ()
     go (Coercion _)                = pure ()
 
-    goA (_, _, e) = go e
+#if MIN_VERSION_GLASGOW_HASKELL(9,1,0,0)
+    goA (Alt _ _ e)
+#else
+    goA (_, _, e)
+#endif
+      = go e
 
 -- | Pretty-print a slice
 pprSlice :: Slice -> SDoc
@@ -155,7 +164,9 @@ eqSlice it slice1 slice2
     essentiallyVar (App e a)  | it, isTyCoArg a = essentiallyVar e
     essentiallyVar (Lam v e)  | it, isTyCoVar v = essentiallyVar e
     essentiallyVar (Cast e _) | it              = essentiallyVar e
-#if MIN_VERSION_ghc(9,0,0)
+#if MIN_VERSION_ghc(9,2,0)
+    essentiallyVar (Case s _ _ [Alt _ _ e]) | it, isUnsafeEqualityProof s = essentiallyVar e
+#elif MIN_VERSION_ghc(9,0,0)
     essentiallyVar (Case s _ _ [(_, _, e)]) | it, isUnsafeEqualityProof s = essentiallyVar e
 #endif
     essentiallyVar (Var v)                      = Just v
@@ -171,7 +182,10 @@ eqSlice it slice1 slice2
 
     go env (Cast e1 _) e2 | it             = go env e1 e2
     go env e1 (Cast e2 _) | it             = go env e1 e2
-#if MIN_VERSION_ghc(9,0,0)
+#if MIN_VERSION_ghc(9,2,0)
+    go env (Case s _ _ [Alt _ _ e1]) e2 | it, isUnsafeEqualityProof s = go env e1 e2
+    go env e1 (Case s _ _ [Alt _ _ e2]) | it, isUnsafeEqualityProof s = go env e1 e2
+#elif MIN_VERSION_ghc(9,0,0)
     go env (Case s _ _ [(_, _, e1)]) e2 | it, isUnsafeEqualityProof s = go env e1 e2
     go env e1 (Case s _ _ [(_, _, e2)]) | it, isUnsafeEqualityProof s = go env e1 e2
 #endif
@@ -218,14 +232,22 @@ eqSlice it slice1 slice2
     go _ _ _ = guard False
 
     -----------
+#if MIN_VERSION_GLASGOW_HASKELL(9,1,0,0)
+    go_alt env (Alt c1 bs1 e1) (Alt c2 bs2 e2)
+#else
     go_alt env (c1, bs1, e1) (c2, bs2, e2)
+#endif
       = guard (c1 == c2) >> go (rnBndrs2 env bs1 bs2) e1 e2
 
-    go_tick :: RnEnv2 -> Tickish Id -> Tickish Id -> Bool
-    go_tick env (Breakpoint lid lids) (Breakpoint rid rids)
+    go_tick :: RnEnv2 -> CoreTickish -> CoreTickish -> Bool
+    go_tick env (Breakpoint{breakpointId = lid, breakpointFVs = lids})
+                (Breakpoint{breakpointId = rid, breakpointFVs = rids})
           = lid == rid  &&  map (rnOccL env) lids == map (rnOccR env) rids
     go_tick _ l r = l == r
 
+#if !MIN_VERSION_GLASGOW_HASKELL(9,1,0,0)
+type CoreTickish = Tickish Id
+#endif
 
 
 -- | Returns @True@ if the given core expression mentions no type constructor
@@ -257,7 +279,12 @@ allTyCons ignore slice =
 
     goB (b, e) = goV b ++ go e
 
-    goA (_,pats, e) = concatMap goV pats ++ go e
+#if MIN_VERSION_GLASGOW_HASKELL(9,1,0,0)
+    goA (Alt _ pats e)
+#else
+    goA (_,pats, e)
+#endif
+      = concatMap goV pats ++ go e
 
     goT (TyVarTy _)      = []
     goT (AppTy t1 t2)    = goT t1 ++ goT t2
@@ -303,7 +330,12 @@ freeOfTerm slice needles = listToMaybe [ (v,e) | (v,e) <- slice, not (go e) ]
 
     goB (_, e) = go e
 
-    goA (ac, _, e) = goAltCon ac && go e
+#if MIN_VERSION_GLASGOW_HASKELL(9,1,0,0)
+    goA (Alt ac _ e)
+#else
+    goA (ac, _, e)
+#endif
+      = goAltCon ac && go e
 
     goAltCon (DataAlt dc) | isNeedle (dataConName dc) = False
     goAltCon _ = True
@@ -350,7 +382,12 @@ doesNotAllocate slice = listToMaybe [ (v,e) | (v,e) <- slice, not (go (idArity v
         -- A let binding allocates if any variable is not a join point and not
         -- unlifted
 
-    goA a (_,_, e) = go a e
+#if MIN_VERSION_GLASGOW_HASKELL(9,1,0,0)
+    goA a (Alt _ _ e)
+#else
+    goA a (_,_, e)
+#endif
+      = go a e
 
 doesNotContainTypeClasses :: Slice -> [Name] -> Maybe (Var, CoreExpr, [TyCon])
 doesNotContainTypeClasses slice tcNs
diff --git a/src/Test/Inspection/Plugin.hs b/src/Test/Inspection/Plugin.hs
index ee16f46..60e7158 100644
--- a/src/Test/Inspection/Plugin.hs
+++ b/src/Test/Inspection/Plugin.hs
@@ -17,7 +17,7 @@ import System.Exit
 import Data.Either
 import Data.Maybe
 import Data.Bifunctor
-import Data.List
+import Data.List (intercalate)
 import qualified Data.Map.Strict as M
 import qualified Language.Haskell.TH.Syntax as TH
 
@@ -29,6 +29,14 @@ import GhcPlugins hiding (SrcLoc)
 import Outputable
 #endif
 
+#if MIN_VERSION_ghc(9,1,0)
+import GHC.Types.TyThing (lookupDataCon)
+#endif
+
+#if MIN_VERSION_ghc(9,3,0)
+import GHC.Utils.Error (mkMCDiagnostic)
+#endif
+
 import Test.Inspection (Obligation(..), Property(..), Result(..))
 import Test.Inspection.Core
 
@@ -319,9 +327,13 @@ proofPass upon_failure report guts = do
         (True, SkipO0) -> pure guts
         (_   , _     ) -> do
             when noopt $ do
+#if MIN_VERSION_GLASGOW_HASKELL(9,3,0,0)
+                msg (mkMCDiagnostic dflags WarningWithoutFlag)
+#else
                 warnMsg
 #if MIN_VERSION_GLASGOW_HASKELL(8,9,0,0)
                     NoReason
+#endif
 #endif
                     $ fsep $ map text
                     $ words "Test.Inspection: Compilation without -O detected. Expect optimizations to fail."
