diff --git a/Simulation/Aivika/Trans/Results/Locale/Types.hs b/Simulation/Aivika/Trans/Results/Locale/Types.hs
index 2b70d92..0abb632 100644
--- a/Simulation/Aivika/Trans/Results/Locale/Types.hs
+++ b/Simulation/Aivika/Trans/Results/Locale/Types.hs
@@ -31,7 +31,7 @@ module Simulation.Aivika.Trans.Results.Locale.Types
         resultNameToTitle) where
 
 import Data.Char
-import Data.List
+import Data.List (groupBy)
 import qualified Data.Map as M
 
 import Simulation.Aivika.Trans.Dynamics
diff --git a/Simulation/Aivika/Trans/Signal.hs b/Simulation/Aivika/Trans/Signal.hs
index 7eea382..f279112 100644
--- a/Simulation/Aivika/Trans/Signal.hs
+++ b/Simulation/Aivika/Trans/Signal.hs
@@ -7,10 +7,10 @@
 -- Stability  : experimental
 -- Tested with: GHC 8.0.1
 --
--- This module defines the signal which we can subscribe handlers to. 
--- These handlers can be disposed. The signal is triggered in the 
--- current time point actuating the corresponded computations from 
--- the handlers. 
+-- This module defines the signal which we can subscribe handlers to.
+-- These handlers can be disposed. The signal is triggered in the
+-- current time point actuating the corresponded computations from
+-- the handlers.
 --
 
 module Simulation.Aivika.Trans.Signal
@@ -64,7 +64,7 @@ module Simulation.Aivika.Trans.Signal
 import Data.Monoid hiding ((<>))
 import Data.Semigroup (Semigroup(..))
 import Data.List.NonEmpty (NonEmpty((:|)))
-import Data.List
+import Data.List (delete)
 import Data.Array
 
 import Control.Monad
@@ -85,15 +85,15 @@ data SignalSource m a =
   SignalSource { publishSignal :: Signal m a,
                                   -- ^ Publish the signal.
                  triggerSignal :: a -> Event m ()
-                                  -- ^ Trigger the signal actuating 
-                                  -- all its handlers at the current 
+                                  -- ^ Trigger the signal actuating
+                                  -- all its handlers at the current
                                   -- simulation time point.
                }
-  
--- | The signal that can have disposable handlers.  
+
+-- | The signal that can have disposable handlers.
 data Signal m a =
   Signal { handleSignal :: (a -> Event m ()) -> Event m (DisposableEvent m)
-           -- ^ Subscribe the handler to the specified 
+           -- ^ Subscribe the handler to the specified
            -- signal and return a nested computation
            -- within a disposable object that, being applied,
            -- unsubscribes the handler from this signal.
@@ -102,7 +102,7 @@ data Signal m a =
 -- | The queue of signal handlers.
 data SignalHandlerQueue m a =
   SignalHandlerQueue { queueList :: Ref m [SignalHandler m a] }
-  
+
 -- | It contains the information about the disposable queue handler.
 data SignalHandler m a =
   SignalHandler { handlerComp :: a -> Event m (),
@@ -117,7 +117,7 @@ instance MonadDES m => Eq (SignalHandler m a) where
 -- To subscribe the disposable handlers, use function 'handleSignal'.
 handleSignal_ :: MonadDES m => Signal m a -> (a -> Event m ()) -> Event m ()
 {-# INLINE handleSignal_ #-}
-handleSignal_ signal h = 
+handleSignal_ signal h =
   do x <- handleSignal signal h
      return ()
 
@@ -127,7 +127,7 @@ handleSignalComposite :: MonadDES m => Signal m a -> (a -> Event m ()) -> Compos
 handleSignalComposite signal h =
   do x <- liftEvent $ handleSignal signal h
      disposableComposite x
-     
+
 -- | Create a new signal source.
 newSignalSource :: MonadDES m => Simulation m (SignalSource m a)
 {-# INLINABLE newSignalSource #-}
@@ -135,7 +135,7 @@ newSignalSource =
   do list <- newRef []
      let queue  = SignalHandlerQueue { queueList = list }
          signal = Signal { handleSignal = handle }
-         source = SignalSource { publishSignal = signal, 
+         source = SignalSource { publishSignal = signal,
                                  triggerSignal = trigger }
          handle h =
            Event $ \p ->
@@ -146,7 +146,7 @@ newSignalSource =
          trigger a =
            triggerSignalHandlers queue a
      return source
-     
+
 -- | Create a new signal source within more low level computation than 'Simulation'.
 newSignalSource0 :: (MonadDES m, MonadRef0 m) => m (SignalSource m a)
 {-# INLINABLE newSignalSource0 #-}
@@ -154,7 +154,7 @@ newSignalSource0 =
   do list <- newRef0 []
      let queue  = SignalHandlerQueue { queueList = list }
          signal = Signal { handleSignal = handle }
-         source = SignalSource { publishSignal = signal, 
+         source = SignalSource { publishSignal = signal,
                                  triggerSignal = trigger }
          handle h =
            Event $ \p ->
@@ -174,8 +174,8 @@ triggerSignalHandlers q a =
   do hs <- invokeEvent p $ readRef (queueList q)
      forM_ hs $ \h ->
        invokeEvent p $ handlerComp h a
-            
--- | Enqueue the handler and return its representative in the queue.            
+
+-- | Enqueue the handler and return its representative in the queue.
 enqueueSignalHandler :: MonadDES m => SignalHandlerQueue m a -> (a -> Event m ()) -> Event m (SignalHandler m a)
 {-# INLINABLE enqueueSignalHandler #-}
 enqueueSignalHandler q h =
@@ -189,14 +189,14 @@ enqueueSignalHandler q h =
 -- | Dequeue the handler representative.
 dequeueSignalHandler :: MonadDES m => SignalHandlerQueue m a -> SignalHandler m a -> Event m ()
 {-# INLINABLE dequeueSignalHandler #-}
-dequeueSignalHandler q h = 
+dequeueSignalHandler q h =
   modifyRef (queueList q) (delete h)
 
 instance MonadDES m => Functor (Signal m) where
 
   {-# INLINE fmap #-}
   fmap = mapSignal
-  
+
 instance MonadDES m => Semigroup (Signal m a) where
 
   {-# INLINE (<>) #-}
@@ -208,10 +208,10 @@ instance MonadDES m => Semigroup (Signal m a) where
   sconcat (x1 :| [x2, x3]) = merge3Signals x1 x2 x3
   sconcat (x1 :| [x2, x3, x4]) = merge4Signals x1 x2 x3 x4
   sconcat (x1 :| [x2, x3, x4, x5]) = merge5Signals x1 x2 x3 x4 x5
-  sconcat (x1 :| (x2 : x3 : x4 : x5 : xs)) = 
+  sconcat (x1 :| (x2 : x3 : x4 : x5 : xs)) =
     sconcat $ merge5Signals x1 x2 x3 x4 x5 :| xs
 
-instance MonadDES m => Monoid (Signal m a) where 
+instance MonadDES m => Monoid (Signal m a) where
 
   {-# INLINE mempty #-}
   mempty = emptySignal
@@ -222,15 +222,15 @@ instance MonadDES m => Monoid (Signal m a) where
   {-# INLINABLE mconcat #-}
   mconcat [] = mempty
   mconcat (h:t) = sconcat (h :| t)
-  
+
 -- | Map the signal according the specified function.
 mapSignal :: MonadDES m => (a -> b) -> Signal m a -> Signal m b
 {-# INLINABLE mapSignal #-}
 mapSignal f m =
-  Signal { handleSignal = \h -> 
+  Signal { handleSignal = \h ->
             handleSignal m $ h . f }
 
--- | Filter only those signal values that satisfy 
+-- | Filter only those signal values that satisfy
 -- the specified predicate.
 filterSignal :: MonadDES m => (a -> Bool) -> Signal m a -> Signal m a
 {-# INLINABLE filterSignal #-}
@@ -239,7 +239,7 @@ filterSignal p m =
             handleSignal m $ \a ->
             when (p a) $ h a }
 
--- | Filter only those signal values that satisfy 
+-- | Filter only those signal values that satisfy
 -- the specified predicate, but then ignoring the values.
 filterSignal_ :: MonadDES m => (a -> Bool) -> Signal m a -> Signal m ()
 {-# INLINABLE filterSignal_ #-}
@@ -247,8 +247,8 @@ filterSignal_ p m =
   Signal { handleSignal = \h ->
             handleSignal m $ \a ->
             when (p a) $ h () }
-  
--- | Filter only those signal values that satisfy 
+
+-- | Filter only those signal values that satisfy
 -- the specified predicate.
 filterSignalM :: MonadDES m => (a -> Event m Bool) -> Signal m a -> Signal m a
 {-# INLINABLE filterSignalM #-}
@@ -257,8 +257,8 @@ filterSignalM p m =
             handleSignal m $ \a ->
             do x <- p a
                when x $ h a }
-  
--- | Filter only those signal values that satisfy 
+
+-- | Filter only those signal values that satisfy
 -- the specified predicate, but then ignoring the values.
 filterSignalM_ :: MonadDES m => (a -> Event m Bool) -> Signal m a -> Signal m ()
 {-# INLINABLE filterSignalM_ #-}
@@ -267,7 +267,7 @@ filterSignalM_ p m =
             handleSignal m $ \a ->
             do x <- p a
                when x $ h () }
-  
+
 -- | Merge two signals.
 merge2Signals :: MonadDES m => Signal m a -> Signal m a -> Signal m a
 {-# INLINABLE merge2Signals #-}
@@ -299,7 +299,7 @@ merge4Signals m1 m2 m3 m4 =
                x3 <- handleSignal m3 h
                x4 <- handleSignal m4 h
                return $ x1 <> x2 <> x3 <> x4 }
-           
+
 -- | Merge five signals.
 merge5Signals :: MonadDES m
                  => Signal m a -> Signal m a -> Signal m a
@@ -320,7 +320,7 @@ mapSignalM :: MonadDES m => (a -> Event m b) -> Signal m a -> Signal m b
 mapSignalM f m =
   Signal { handleSignal = \h ->
             handleSignal m (f >=> h) }
-  
+
 -- | Transform the signal.
 apSignal :: MonadDES m => Event m (a -> b) -> Signal m a -> Signal m b
 {-# INLINABLE apSignal #-}
@@ -336,7 +336,7 @@ emptySignal =
 
 -- | Represents the history of the signal values.
 data SignalHistory m a =
-  SignalHistory { signalHistorySignal :: Signal m a,  
+  SignalHistory { signalHistorySignal :: Signal m a,
                   -- ^ The signal for which the history is created.
                   signalHistoryTimes  :: Ref m [Double],
                   signalHistoryValues :: Ref m [a] }
@@ -368,7 +368,7 @@ newSignalHistoryStartingWith init signal =
      return SignalHistory { signalHistorySignal = signal,
                             signalHistoryTimes  = ts,
                             signalHistoryValues = xs }
-       
+
 -- | Read the history of signal values.
 readSignalHistory :: MonadDES m => SignalHistory m a -> Event m (Array Int Double, Array Int a)
 {-# INLINABLE readSignalHistory #-}
@@ -378,7 +378,7 @@ readSignalHistory history =
      let n  = length xs0
          xs = listArray (0, n - 1) (reverse xs0)
          ys = listArray (0, n - 1) (reverse ys0)
-     return (xs, ys)     
+     return (xs, ys)
 
 -- | Trigger the signal with the current time.
 triggerSignalWithCurrentTime :: MonadDES m => SignalSource m Double -> Event m ()
@@ -393,7 +393,7 @@ newSignalInTimes xs =
   do s <- liftSimulation newSignalSource
      enqueueEventWithTimes xs $ triggerSignalWithCurrentTime s
      return $ publishSignal s
-       
+
 -- | Return a signal that is triggered in the integration time points.
 -- It should be called with help of 'runEventInStartTime'.
 newSignalInIntegTimes :: MonadDES m => Event m (Signal m Double)
@@ -402,7 +402,7 @@ newSignalInIntegTimes =
   do s <- liftSimulation newSignalSource
      enqueueEventWithIntegTimes $ triggerSignalWithCurrentTime s
      return $ publishSignal s
-     
+
 -- | Return a signal that is triggered in the start time.
 -- It should be called with help of 'runEventInStartTime'.
 newSignalInStartTime :: MonadDES m => Event m (Signal m Double)
@@ -486,7 +486,7 @@ appendSignalable m1 m2 =
 -- saving the information about the time points at which the original signal was received.
 arrivalSignal :: MonadDES m => Signal m a -> Signal m (Arrival a)
 {-# INLINABLE arrivalSignal #-}
-arrivalSignal m = 
+arrivalSignal m =
   Signal { handleSignal = \h ->
              do r <- liftSimulation $ newRef Nothing
                 handleSignal m $ \a ->
@@ -511,7 +511,7 @@ delaySignal delta m =
                h <- handleSignal m $ \a ->
                  Event $ \p ->
                  invokeEvent p $
-                 enqueueEvent (pointTime p + delta) $ 
+                 enqueueEvent (pointTime p + delta) $
                  do x <- readRef r
                     unless x $ h a
                return $ DisposableEvent $
@@ -529,7 +529,7 @@ delaySignalM delta m =
                  Event $ \p ->
                  do delta' <- invokeEvent p delta
                     invokeEvent p $
-                      enqueueEvent (pointTime p + delta') $ 
+                      enqueueEvent (pointTime p + delta') $
                       do x <- readRef r
                          unless x $ h a
                return $ DisposableEvent $
@@ -538,7 +538,7 @@ delaySignalM delta m =
          }
 
 -- | Show the debug message with the current simulation time.
-traceSignal :: MonadDES m => String -> Signal m a -> Signal m a 
+traceSignal :: MonadDES m => String -> Signal m a -> Signal m a
 {-# INLINABLE traceSignal #-}
 traceSignal message m =
   Signal { handleSignal = \h ->
