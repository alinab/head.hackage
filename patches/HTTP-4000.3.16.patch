diff --git a/Network/HTTP/Cookie.hs b/Network/HTTP/Cookie.hs
index 1b5175d..2c4c72e 100644
--- a/Network/HTTP/Cookie.hs
+++ b/Network/HTTP/Cookie.hs
@@ -3,14 +3,14 @@
 -- Module      :  Network.HTTP.Cookie
 -- Copyright   :  See LICENSE file
 -- License     :  BSD
--- 
+--
 -- Maintainer  :  Ganesh Sittampalam <ganesh@earth.li>
 -- Stability   :  experimental
 -- Portability :  non-portable (not tested)
 --
 -- This module provides the data types and functions for working with HTTP cookies.
 -- Right now, it contains mostly functionality needed by 'Network.Browser'.
--- 
+--
 -----------------------------------------------------------------------------
 module Network.HTTP.Cookie
        ( Cookie(..)
@@ -24,7 +24,7 @@ module Network.HTTP.Cookie
 import Network.HTTP.Headers
 
 import Data.Char
-import Data.List
+import Data.List (intercalate, isPrefixOf, isSuffixOf)
 import Data.Maybe
 
 import Text.ParserCombinators.Parsec
@@ -38,8 +38,8 @@ import Text.ParserCombinators.Parsec
 
 -- | @Cookie@ is the Haskell representation of HTTP cookie values.
 -- See its relevant specs for authoritative details.
-data Cookie 
- = MkCookie 
+data Cookie
+ = MkCookie
     { ckDomain  :: String
     , ckName    :: String
     , ckValue   :: String
@@ -50,8 +50,8 @@ data Cookie
     deriving(Show,Read)
 
 instance Eq Cookie where
-    a == b  =  ckDomain a == ckDomain b 
-            && ckName a == ckName b 
+    a == b  =  ckDomain a == ckDomain b
+            && ckName a == ckName b
             && ckPath a == ckPath b
 
 -- | @cookieToHeaders ck@ serialises @Cookie@s to an HTTP request header.
@@ -66,7 +66,7 @@ mkCookieHeaderValue = intercalate "; " . map mkCookieHeaderValue1
     mkCookieHeaderValue1 c = ckName c ++ "=" ++ ckValue c
 
 -- | @cookieMatch (domain,path) ck@ performs the standard cookie
--- match wrt the given domain and path. 
+-- match wrt the given domain and path.
 cookieMatch :: (String, String) -> Cookie -> Bool
 cookieMatch (dom,path) ck =
  ckDomain ck `isSuffixOf` dom &&
@@ -75,13 +75,13 @@ cookieMatch (dom,path) ck =
    Just p  -> p `isPrefixOf` path
 
 
--- | @processCookieHeaders dom hdrs@ 
+-- | @processCookieHeaders dom hdrs@
 processCookieHeaders :: String -> [Header] -> ([String], [Cookie])
 processCookieHeaders dom hdrs = foldr (headerToCookies dom) ([],[]) hdrs
 
--- | @headerToCookies dom hdr acc@ 
+-- | @headerToCookies dom hdr acc@
 headerToCookies :: String -> Header -> ([String], [Cookie]) -> ([String], [Cookie])
-headerToCookies dom (Header HdrSetCookie val) (accErr, accCookie) = 
+headerToCookies dom (Header HdrSetCookie val) (accErr, accCookie) =
     case parse cookies "" val of
         Left{}  -> (val:accErr, accCookie)
         Right x -> (accErr, x ++ accCookie)
@@ -100,11 +100,11 @@ headerToCookies dom (Header HdrSetCookie val) (accErr, accCookie) =
           return $ mkCookie name val1 args
 
    cvalue :: Parser String
-   
+
    spaces_l = many (satisfy isSpace)
 
    cvalue = quotedstring <|> many1 (satisfy $ not . (==';')) <|> return ""
-   
+
    -- all keys in the result list MUST be in lower case
    cdetail :: Parser [(String,String)]
    cdetail = many $
@@ -118,7 +118,7 @@ headerToCookies dom (Header HdrSetCookie val) (accErr, accCookie) =
            )
 
    mkCookie :: String -> String -> [(String,String)] -> Cookie
-   mkCookie nm cval more = 
+   mkCookie nm cval more =
           MkCookie { ckName    = nm
                    , ckValue   = cval
                    , ckDomain  = map toLower (fromMaybe dom (lookup "domain" more))
@@ -128,7 +128,7 @@ headerToCookies dom (Header HdrSetCookie val) (accErr, accCookie) =
                    }
 headerToCookies _ _ acc = acc
 
-      
+
 
 
 word, quotedstring :: Parser String
